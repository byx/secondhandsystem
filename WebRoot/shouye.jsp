<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商城首页</title>
<link rel="stylesheet" href="css/public.css" />
<link rel="stylesheet" href="css/index.css" />
<link rel="stylesheet" href="css/layui.css" />
<link rel="stylesheet" href="css/loginctn.css" />
<link rel="stylesheet" href="css/reset(fenye).css" />
<link rel="stylesheet" href="css/pagination.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<script type="text/javascript" src="js/highlight.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</head>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>商城首页</title>
<link rel="stylesheet" href="css/public.css" />
<link rel="stylesheet" href="css/index.css" />
<link rel="stylesheet" href="css/layui.css" />
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.pagination.js"></script>
<script type="text/javascript" src="js/highlight.min.js"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/index.js"></script>
</head>

<body>
	<jsp:include page="header.jsp"></jsp:include>
	<div id="header-bottom">
		<div class="header-bottom-wrap clearfix">
			<div class="city fl">
				<a href="javascript:;">校园<i class="icon"></i></a>
			</div>
			<div class="search-wrap fl">
				<div class="search">
					<input type="text" class="keywords" placeholder="二手市场交易"> <a
						href="javascript:;" class="search-btn search-normal-btn">搜索</a>
				</div>
			</div>

			<div class="publish fr">
				<a href="#" class="publish-btn"><i class="icon"></i>发布商品</a>
			</div>
		</div>
	</div>
	<div id="main" class="clearfix">
		<div class="category fl">
			<ul>

				<li><a href="#">手机<i class="icon">></i></a></li>

				<li><a href="sortcomputer.jsp">电脑<i class="icon">></i></a></li>

				<li><a href="#">配件<i class="icon">></i></a></li>

				<li><a href="#">电器<i class="icon">></i></a></li>

				<li><a href="#">书籍<i class="icon">></i></a></li>

				<li><a href="#">娱乐<i class="icon">></i></a></li>

				<li><a href="#">运动<i class="icon">></i></a></li>

				<li><a href="#">代步<i class="icon">></i></a></li>

				<!--li><a class="shuang11" target="_blank" href="https://s.click.taobao.com/8gaPEZw"><img src="/images/activity/index_nav_img.jpg" /></a></li-->
			</ul>
		</div>
		<div class="main-box fl">
			<div class="banner">
				<div class="focusMap fl">
					<span class="prev" style="display: none;"><i class="icon"></i></span>
					<span class="next" style="display: none;"><i class="icon"></i></span>
					<ul class="rImg">

						<li style="display: none;"><a href="#" alt="百度云学生服务器"><img
								src="img/head1.jpg"></a></li>

						<li style="display: none;"><a href="#" alt="知识就是力量"><img
								src="img/head2.jpg"></a></li>

						<li style="display: list-item;"><a href="#" target="_blank"><img
								src="img/head3.jpg" alt="创业技术服务"></a></li>

					</ul>

					<ul class="button">

						<li class=""></li>

						<li class=""></li>

						<li class="on"></li>

					</ul>

				</div>
			</div>
			<div class="index-list">
				<div class="list-header">
					<a href="/" class="active">推荐</a> <a href="/?n=true" class="">最新</a>
					<a href="/publish" class="fr"><i class="icon"></i>发布信息</a>
				</div>

				<div class="list-body">
					<!--  *******************************jquery导入********************************************** -->
					<ul class="clearfix" id="clearfix"></ul>
				</div>
				<div id="page">
					<div class="m-style M-box11"></div>
				</div>
			</div>
		</div>
		<div class="sidebar fr">
			<div class="sidebar-banner-right">
				<div class="wei clearfix">
					<div class="wei-left fl">
						<a href="javascript:;" class="weixin"><img
							src="img/weixin.png"></a> <a
							href="http://weibo.com/taoertaocom" target="_blank" class="weibo"><img
							src="img/weibo.png"></a>
					</div>
					<div class="wei-right fr">
						<img src="img/weibo.png" alt=""> <span class="triangle"></span>
					</div>
				</div>
				<div class="app-download">
					<p>手机 APP 下载</p>
					<a href="javascript:;">Download</a>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer.jsp"></jsp:include>
	<script type="text/javascript">
		$(function() {
			var page; // 总页数
			var prov = 1;
			$.getJSON("goods/main", function(data) {
				page = data.totalPage;
				var list = data.lists;
				var str = "";
				for (var i = 0; i < list.length; i++) {
					str += "<li><a href='/detail/1931' target='_blank' class='good-image'><img class='image-show-box' src='" + list[i].g_img + "'>" +
						"</a><a href='/detail/1931' target='_blank' class='good-title'>" + list[i].text + "</a>" +
						"<span class='good-price'>" + list[i].g_price + "</span><span class='pub-time fr'>发布于 " + list[i].create_time + "</span></li>";
				}
				<%-- console.log(str);
				alert(<%=session.getAttribute("totalPage")%>); --%>
				$("#clearfix").html(str);
			});
			$(".M-box11").pagination({
				mode : 'fixed',
				pageCount : <%=session.getAttribute("totalPage")%>
			});
			$(".M-box11").click(function() {
				var next = $(".M-box11 span").html();
				if (next != prov) {
					$.getJSON("goods/main2",{"currentPage":next}, function(data) {
						var list = data;
						var str = "";
						for (var i = 0; i < list.length; i++) {
							str += "<li><a href='/detail/1931' target='_blank' class='good-image'><img class='image-show-box' src='" + list[i].g_img + "'>" +
								"</a><a href='/detail/1931' target='_blank' class='good-title'>" + list[i].text + "</a>" +
								"<span class='good-price'>" + list[i].g_price + "</span><span class='pub-time fr'>发布于 " + list[i].create_time + "</span></li>";
						}
						$("#clearfix").html(str);
					});
					prov = next;
				} 
			});
		});
	</script>
</body>
</html>
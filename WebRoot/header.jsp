<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'header.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>

<body>
	<div id="header">
		<div class="header-wrap">
			<a href="/" class="logo fl"> <img src="img/new-logo.png">
			</a>
			<ul class="nav fl">
				<li><a href="shouye.jsp">首页</a></li>
				<li><a href="#" target="_blank" class="yhq">个人信息</a></li>
				<li><a href="#" target="_blank">购物车</a></li>
				<li><a href="#" target="_blank">收藏夹</a></li>
				<li><a href="#" target="_blank">联系我们</a></li>
			</ul>
			<div class="nav-right fr">
				<a href="register.jsp" class="log-btn">注册</a> <a class="log-btn"
					href="login.jsp">登录</a>
			</div>
		</div>
	</div>
</body>
</html>

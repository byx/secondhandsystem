<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>

	<head>
		<base href="<%=basePath%>">

		<title>My JSP 'register.jsp' starting page</title>

		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
		<link rel="stylesheet" href="css/reset.css" />
		<link rel="stylesheet" href="css/common.css" />
		<link rel="stylesheet" href="css/font-awesome.min.css" />

	</head>

	<body>
		<div class="wrap login_wrap">
			<div class="content">

				<div class="logo"></div>

				<div class="login_box">

					<div class="login_form">
						<div class="login_title">注册</div>
						<form action="#" method="post" id="fsb">

							<div class="form_text_ipt">
								<input name="username" id="phone" type="text" placeholder="手机号 "><span class="rights" id="phoneright">该手机号码可用</span>
							</div>
							<div class="ececk_warning">
								<span class="sjz">手机号不能为空</span>
							</div>
							<div class="form_text_ipt">
								<input name="password" id="password" type="password" placeholder="密码需6-16位"><span class="rights" id="passwordright">密码可用</span>
							</div>
							<div class="ececk_warning">
								<span class="mmz">密码不能为空</span>
							</div>
							<div class="form_text_ipt">
								<input name="repassword" id="password2" type="password" placeholder="重复密码"><span class="rights" id="password2right">密码输入正确</span>
							</div>
							<div class="ececk_warning">
								<span class="mmz2">重复密码填写不正确</span>
							</div>
							<div class="form_text_iptdd">
								<input name="code" id="code" type="text" placeholder="验证码"><span class="rights" id="yzmright">该验证码正确</span>
							</div>
							<input type="button" value="点击获取验证码" class="yzm" />
							<div class="ececk_warnings">
								<span>请填写正确的验证码</span>
							</div>
							<div class="form_btn">
								<button type="button" id="zhuce" style="margin-top: 70px;display: block;">注册</button>
							</div>
							<div class="form_reg_btn">
								<span>已有帐号？</span>
								<a href="login.jsp">马上登录</a>
							</div>
						</form>
						<div class="other_login">
							<div class="left other_left">
								<span>其它登录方式</span>
							</div>
							<div class="right other_right">
								<a href="#"><i class="fa fa-qq fa-2x"></i></a>
								<a href="#"><i class="fa fa-weixin fa-2x"></i></a>
								<a href="#"><i class="fa fa-weibo fa-2x"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="js/jquery.min.js"></script>
		<script type="text/javascript" src="js/common.js"></script>
		<script type="text/javascript">
			$(function() {
				$(".yzm").click(function() {
					var phone = $("#phone").val();
					// 给电话号码正则校验，防止乱填写
					if(!(/^1[3456789]\d{9}$/.test(phone))) {
						alert("电话号码格式不正确");
						return;
					} else {
						/* 按钮倒计时 */
						var $sendCheck = $(".yzm"); // 获得节点
						var timer = null; //
						var waitTime = 5; // 等待时间
						var checkTime = waitTime; // 按钮上的时间
						var setCheckText = function() {
							if(checkTime <= 0) {
								$sendCheck.val('获取验证码');
								$(".yzm").css("background", "#FF7F50");
							} else {
								$sendCheck.val('(' + checkTime + '秒)重发');
								$(".yzm").css("background", "#57565f");
							}
						};
						$sendCheck.attr('disabled', true);
						setCheckText();
						timer = setInterval(function() {
							checkTime--;
							setCheckText();
							if(checkTime === 0) {
								clearInterval(timer);
								$sendCheck.attr('disabled', false);
								checkTime = waitTime;
							}
						}, 1000);
						/* ajax传送手机号 */
						$.getJSON("user/code", {
							"phone": phone
						}, function(data) {
							// 如果返回正确的值，则意味着验证码发送成功
							if(data == true) {
								alert("验证码已发送!");
							} else {
								alert("验证码发送失败！");
							}
						});
					}

				});

				/* 获得焦点+样式 */
				$("input[type='text']").focus(function() {
					$(this).css("background-color", "#FFFFCC");
				});

				$("input[type='password']").focus(function() {
					$(this).css("background-color", "#FFFFCC");
				});

				/* input失去焦点+标记 */
				var flag = false;
				$("#phone").blur(function() {
					var phone = $("#phone").val();
					$("#phone").css("background-color", "white");
					if(!(/^1[3456789]\d{9}$/.test(phone))) {
						$(".sjz").show();
						$("#phoneright").hide();
						$(".sjz").html("手机号码格式不正确!");
						flag = false;
					} else {
						$("#phoneright").show();
						$(".sjz").hide();
						flag = true;
					}
				});

				$("#password").blur(function() {
					var mi = $("#password").val();
					$("#password").css("background-color", "white");
					if(mi.length > 5 && mi.length < 17) {
						$("#passwordright").show();
						$(".mmz").hide();
						flag = true;
					} else {
						$(".mmz").show();
						$("#passwordright").hide();
						flag = false;
					}
				})

				$("#password2").blur(function() {
					var p1 = $("#password").val();
					var p2 = $("#password2").val();
					$("#password2").css("background-color", "white");
					if(p1 != p2 || p2 == "") {
						$(".mmz2").show();
						$("#password2right").hide();
						flag = false;
					} else {
						$(".mmz2").hide();
						$("#password2right").show();
						flag = true;
					}
				})

				$("#code").blur(function() {
					$("#code").css("background-color", "white");
					var codes = $("#code").val();
					var code2 = '<%=session.getAttribute("code")%>';
					if(code2 == "" || codes != code2) {
						$(".ececk_warnings").show();
						$("#yzmright").hide();
						flag = false;
					} else {
						$(".ececk_warnings").hide();
						//$("#yzmright").show();
						flag = true;
					}
				});


				$(document).keydown(function(event){
 				if(event.keyCode == "13"){
 					$("#zhuce").click();
 				}
 			});

				$("#zhuce").click(function() {

					/* 给4个input失去焦点方法*/
					$("#phone").blur();
					$("#password").blur();
					$("#password2").blur();
					$("#code").blur();

					if(flag) {
						if(confirm("确认注册吗？")) {
							//window.location ="http://baidu.com"	
							var phone2 = '<%=session.getAttribute("phone")%>';
							var codes = $("#code").val();

							//手机验证码判断
							$.getJSON("user/querybyphone", {
								"phone": phone2
							}, function(data) {
								if(data == true) {
									var password = $("#password").val();
									$.getJSON("user/add", {
										"phone": phone2,
										"password": password
									}, function(data) {
										if(data == true) {
											//alert("注册成功!5s后跳转登录页面。。。");
											/* 注册成功，在这里转发 */
											window.location.href ="tiaozhuan.jsp";
											
											
										} else {
											alert("数据库插入错误！");
										}
									});
								} else {
									alert("用户已存在，请返回登录页登录");
								}
							}); //querybyphone end
						}
					}
				});
			});
		</script>
	</body>

</html>
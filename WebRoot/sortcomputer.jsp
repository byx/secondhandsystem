<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html lang="en">

	<head>
		<meta charset="UTF-8">

		<title>电脑-分类列表-甜狗二手网</title>
		<meta name="keywords" content="二手电脑,学生用电脑,二手电脑交易,校园二手电脑">
		<meta name="description" content="淘二淘-大学生二手网，二手电脑交易信息发布频道，为你提供优质真实可靠的学生二手电脑交易信息。">
		<link rel="stylesheet" href="css/public.css" />
		<link rel="stylesheet" href="css/category.css" />
		<link rel="stylesheet" href="css/layui.css" />
		<link rel="stylesheet" href="css/loginctn.css" />
		<script type="text/javascript" src="js/jquery-3.3.1.min.js" ></script>
	</head>
	
		<body>
			<jsp:include page="header.jsp"></jsp:include>
			<div id="header-bottom">
				<div class="header-bottom-wrap clearfix">
					<div class="city fl">
						<a href="javascript:;">全球<i class="icon">&#xe629;</i></a>
					</div>
					<div class="search-wrap fl">
						<div class="search">
							<input type="text" class="keywords" placeholder="请输入关键词">
							<a href="javascript:;" class="search-btn search-normal-btn">搜索</a>
						</div>
					</div>
					<!--<div class="hot-search fl">
			<div class="hot-search-title">热搜：</div>
			<div class="hot-search-words">
				<script type="text/javascript">
					/*新版热门搜索*/
					var cpro_id = "u2955274";
				</script>
				<script type="text/javascript" src="http://cpro.baidustatic.com/cpro/ui/c.js"></script>
			</div>
		</div>-->
					<div class="publish fr">
						<a href="/publish" class="publish-btn"><i class="icon">&#xe615;</i>发布商品</a>
					</div>
				</div>
			</div>
			<div id="main" class="clearfix">
				<div class="cate-name-list">
					<!-- <span>分类：</span> -->
					<a class="" href="/category_list/0">全部分类</a>

					<a class="" href="/category_list/1">手机</a>

					<a class="active" href="/category_list/2">电脑</a>

					<a class="" href="/category_list/4">配件</a>

					<a class="" href="/category_list/5">电器</a>

					<a class="" href="/category_list/6">书籍</a>

					<a class="" href="/category_list/7">娱乐</a>

					<a class="" href="/category_list/8">运动</a>

					<a class="" href="/category_list/9">代步</a>

				</div>
				<div style="margin-top: 20px;">
					<!--<script type="text/javascript">
				/*分类列表顶部*/
				var cpro_id = "u3184170";
			</script>
			<script type="text/javascript" src="//cpro.baidustatic.com/cpro/ui/c.js"></script>-->
					<script type="text/javascript" language="javascript" charset="utf-8" src="//static.mediav.com/js/mvf_g2.js"></script>
				</div>
				<div class="list-body">
					<ul class="clearfix hei">
						<!-- <li>
					<a href="/detail" class="good-image"><img src="http://img03.taobaocdn.com/bao/uploaded/i3/TB1TN9JOpXXXXasXpXXXXXXXXXX_!!0-item_pic.jpg" alt=""></a>
					<a href="" class="good-title">标题标题标题标题标题标题标题标题标题标题</a>
					<span class="good-price">￥200.0</span>
				</li> -->

						<li>
							<a class="good-image" target="_blank"><img src="images/goods/computer/computer (2).jpg" alt="全新曲面24英寸液晶显示器22台式电脑显示屏27高清电竞32屏幕ps4"></a>
							<a class="good-title" target="_blank">全新曲面24英寸液晶显示器22台式电脑显示屏27高清电竞32屏幕ps4</a>
							<span class="good-price">￥386.00</span>

						</li>

						
					</ul>
					<div id="page">
						<div class="layui-box layui-laypage layui-laypage-default" id="layui-laypage-0">
							<a href="javascript:;" class="layui-laypage-prev" data-page="1">上一页</a>
							<a href="javascript:;" class="layui-laypage-fitst" title="首页" data-page="6">首页</a>
							<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em>1</em></span>
							<a href="javascript:;" data-page="1">2</a>
							<a href="javascript:;" data-page="3">3</a>
							<a href="javascript:;" data-page="4">4</a>
							<a href="javascript:;" data-page="5">5</a><span>…</span>
							<a href="javascript:;" class="layui-laypage-last" title="尾页" data-page="6">末页</a>
							<a href="javascript:;" class="layui-laypage-next" data-page="3">下一页</a>
						</div>
					</div>
				</div>
			</div>
	<jsp:include page="footer.jsp"></jsp:include>
			<script src="/js/lib/sitemap.js"></script>
			<script>
				$(function() {
					layui.use(['layer', 'laypage'], function() {
						var layer = layui.layer

						var idArr = location.pathname.split("/")
						layui.laypage({
							cont: 'page', //容器。值支持id名、原生dom对象，jquery对象。【如该容器为】：<div id="page1"></div>
							pages: 6, //通过后台拿到的总页数
							curr: Common.getQuery('page') || 1, //当前页
							jump: function(obj, first) { //触发分页后的回调
								if(!first) { //点击跳页触发函数自身，并传递当前页：obj.curr
									window.location.href = '/category_list/' + idArr[idArr.length - 1] + '?page=' + obj.curr
								}
							}
						})
					})
				})
			</script>
		</body>

</html>
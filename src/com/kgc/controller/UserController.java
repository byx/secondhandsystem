package com.kgc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.aliyuncs.exceptions.ClientException;
import com.kgc.entity.User;
import com.kgc.service.UserService;
import com.kgc.util.RandomNumber;
import com.kgc.util.SmsDemo;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {
	
	@Autowired
	private UserService userLoginService;
	
	@RequestMapping("login")
	public boolean login(String phone, String password, HttpServletRequest req){
		User user = userLoginService.login(phone, password);
		if(user != null){
			HttpSession session = req.getSession();
			session.setAttribute("user", user);
			return true;
		}			
		return false;
	}
	
	@RequestMapping("code")
	public void getVerificationCode(String phone,HttpServletRequest req){
		RandomNumber rdn = new RandomNumber();
		String codeNum = rdn.sixNumber();
		HttpSession session = req.getSession();
		session.setAttribute("phone", phone);
		session.setAttribute("code", codeNum);
		try {
			SmsDemo.sendSms(phone, codeNum);
			Thread.sleep(3000L);
		} catch (ClientException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	
	@RequestMapping("querybyphone")
	public boolean registUser(String phone){
		boolean flag =  userLoginService.registUser(phone);
		System.out.println(flag);
		return flag;
	}
	
	@RequestMapping("add")
	public boolean add(String phone,String password){
		
		return userLoginService.add(phone, password);
	}
	
	
	
}

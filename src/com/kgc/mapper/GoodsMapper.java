package com.kgc.mapper;

import java.util.HashMap;
import java.util.List;


import com.kgc.entity.Goods;

public interface GoodsMapper {
	
	/**
	 * 分页
	 * @param map
	 * @return
	 */
	public List<Goods> findByPage(HashMap<String, Object> map);
	
	/**
     * 查询用户记录总数
     * @return
     */
    public int selectCount();
    
    /**
     * 查询所有用户数据
     * @return
     */
    public List<Goods> selectUserList();

}

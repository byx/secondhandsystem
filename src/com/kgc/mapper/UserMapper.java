package com.kgc.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.kgc.entity.Goods;
import com.kgc.entity.User;

public interface UserMapper {
	
	public User login(@Param("phone") String phone,@Param("password") String password);
	
	public List<User> regist(@Param("phoneNum")String phone);
	
	public int add(@Param("phone") String phone,@Param("password") String password);
	
	
}

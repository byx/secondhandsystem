package com.kgc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kgc.entity.User;
import com.kgc.mapper.UserMapper;
import com.kgc.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserMapper userLoginMapper;
	
	public User login(String phone,String password){
		return  userLoginMapper.login(phone, password);
	}

	@Override
	public boolean registUser(String phone) {
		List<User> list = userLoginMapper.regist(phone);
		System.out.println(list.size()==0);
		return list.size()==0?true:false;
	}

	@Override
	public boolean add(String phone, String password) {
		return userLoginMapper.add(phone,password)>=1?true:false;
	}

	
	
}

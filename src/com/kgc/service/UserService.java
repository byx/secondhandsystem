package com.kgc.service;

import com.kgc.entity.User;

public interface UserService {
	
	public User login(String phone,String password);
	
	public boolean registUser(String phone);
	
	public boolean add(String phone,String password);
	
	
}

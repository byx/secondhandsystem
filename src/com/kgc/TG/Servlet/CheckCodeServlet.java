package com.kgc.TG.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CheckCodeServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	 protected void doPost(HttpServletRequest req, HttpServletResponse resp)
	            throws ServletException, IOException {
		resp.setContentType("text/html;charset=UTF-8");
		req.setCharacterEncoding("utf-8");
		 PrintWriter out = resp.getWriter();
		 // 获取存放在session中的验证码
	        String code = (String) req.getSession().getAttribute("code");
	        // 获取页面提交的验证码
	        String inputCode = req.getParameter("code");
	        if(code.toLowerCase().equals(inputCode.toLowerCase())) { // 验证码不区分大小写
	            // 验证成功，跳转到成功页面
	        	out.print("<script type=\"text/javascript\">");
				out.print("alert(\"密码修改成功！\");");
				out.print("location.href=\"" + req.getContextPath() + "/pages/Personal_information.jsp" + "\";");
				out.print("</script>");
	        } else { // 验证失败
	        	out.print("<script type=\"text/javascript\">");
				out.print("alert(\"验证码输入错误！点击确认返回原来页面\");");
				out.print("location.href=\"" + req.getContextPath() + "/pages/UpdatePwd.jsp" + "\";");
				out.print("</script>");
	        }
	    }


}
